/* 
    ------------------- Code Monkey -------------------

    Thank you for downloading this package
    I hope you find it useful in your projects
    If you have any questions let me know
    Cheers!

               unitycodemonkey.com
    --------------------------------------------------
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;

using System.IO;
using System.Text;
using UnityEngine.UI;
using TMPro;

public class Assistant : MonoBehaviour {
    public AudioManagerBtnCtl audioCtl;

    [SerializeField] private Transform jokeBtn;
    //[Header("SetTrueFalse")]
    [SerializeField] private Transform setTFBtn;
    [SerializeField] private Transform TFBtn;
    [SerializeField] private Transform TFBtnInputField1;
    public List<Transform> inputFieldsBtns=new List<Transform>();

    //[Header("File")]
    string m_Str;
    string m_FileName;                             //法二要用
    string[] strs;                                        //法二要用

    public TextMeshPro textMeshPro;   //顯示Code UI

    [Header("PublicInput")]
    public List<TextMeshPro> inputFields=new List<TextMeshPro>();  //InputF TMP容器
    public List<string> TFinputstrings=new List<string>();   //InputF string容器
    public int nowInputF=0;    //現在點選的InputF

    bool changeLetter=false;
    
    //[Heade("TF Attribute")]
    bool TFtruefalse=true;
    public TextMeshPro TFdisplayText;

    bool activeInput=false;
    public List<bool> activeInputs=new List<bool>();


    public TextMeshPro TFinputFieldText;
    string m_TFinputstrs;
    string[] TFinputstrs;


    [Header("顯示開關")]
    [SerializeField] private Transform CtlCtlBtn;
    [SerializeField] private Transform BtnCtlBtn;
    [SerializeField] private Transform IFCtlBtn;
    [SerializeField] private Transform DisplayCtlBtn;
    [SerializeField] private Transform CloseInputBtn;
    public List<GameObject> displayCtl=new List<GameObject>();
    public List<GameObject> btnCtl=new List<GameObject>();
    public List<GameObject> inputFieldCtl=new List<GameObject>();
    public List<GameObject> ctlCtl=new List<GameObject>();

    bool controlctlBool=false;
    bool displayBool=false;
    bool btnBool=false;
    bool inputFieldBool=false;

    [Header("AudioManager")]
    [SerializeField] private Transform audioBtn;
    [SerializeField] private Transform audioPSBtn;
    bool playstop=true;
    public TextMeshPro PSdisplayText;

    private void Start() {
        CloseActiveInputs();

        jokeBtn.GetComponent<Button_Sprite>().ClickFunc = () => {
            ChatBubble.Create(null, new Vector3(15.0f, -7.0f), ChatBubble.IconType.Happy, GetJoke());
        };
        #region TF
        setTFBtn.GetComponent<Button_Sprite>().ClickFunc = () => {      //TF總輸出
            CloseActiveInputs();
            //TextBtn(TFinputFieldText.text,TFtruefalse);
            TextBtn(inputFields[nowInputF].text,TFtruefalse);
        };
        TFBtn.GetComponent<Button_Sprite>().ClickFunc = () => {         //TF變換
            TFtruefalse=!TFtruefalse;
            if(TFtruefalse==true)
            {
                TFdisplayText.SetText("T");
                TFdisplayText.ForceMeshUpdate();
            }
            else
            {
                TFdisplayText.SetText("F");
                TFdisplayText.ForceMeshUpdate();
            }
        };
        #endregion

        #region inputField
        inputFieldsBtns[0].GetComponent<Button_Sprite>().ClickFunc = () => {
            nowInputF=0;
            //m_TFinputstrs="";
            TFinputstrings[nowInputF]="";
            //activeInput=true;
            activeInputs[nowInputF]=true;            
            // FunctionTimer.Create(()=>{
            //     activeInput=false;
            // },5.0f);            
        };
        inputFieldsBtns[1].GetComponent<Button_Sprite>().ClickFunc = () => {
            nowInputF=1;
            TFinputstrings[nowInputF]="";
            activeInputs[nowInputF]=true;            
        };
        inputFieldsBtns[2].GetComponent<Button_Sprite>().ClickFunc = () => {
            nowInputF=2;
            TFinputstrings[nowInputF]="";
            activeInputs[nowInputF]=true;            
        };
        inputFieldsBtns[3].GetComponent<Button_Sprite>().ClickFunc = () => {
            nowInputF=3;
            TFinputstrings[nowInputF]="";
            activeInputs[nowInputF]=true;            
        };
        inputFieldsBtns[4].GetComponent<Button_Sprite>().ClickFunc = () => {
            nowInputF=4;
            TFinputstrings[nowInputF]="";
            activeInputs[nowInputF]=true;            
        };
        #endregion
        DisplayCtlBtn.GetComponent<Button_Sprite>().ClickFunc = () => {      //TF總輸出
            displayBool=!displayBool;
            ControlAll(displayCtl,displayBool);
        };
        BtnCtlBtn.GetComponent<Button_Sprite>().ClickFunc = () => {      //TF總輸出
            btnBool=!btnBool;
            ControlAll(btnCtl,btnBool);
        };
        IFCtlBtn.GetComponent<Button_Sprite>().ClickFunc = () => {      //TF總輸出
            inputFieldBool=!inputFieldBool;
            ControlAll(inputFieldCtl,inputFieldBool);
        };
        CtlCtlBtn.GetComponent<Button_Sprite>().ClickFunc = () => {      //TF總輸出
            controlctlBool=!controlctlBool;
            ControlAll(ctlCtl,controlctlBool);
        };

        CloseInputBtn.GetComponent<Button_Sprite>().ClickFunc = () => {      //TF總輸出
            CloseActiveInputs();
        };



        #region Audio
        audioBtn.GetComponent<Button_Sprite>().ClickFunc = () => {      //TF總輸出
            CloseActiveInputs();
            //TextBtn(TFinputFieldText.text,TFtruefalse);
            audioCtl.TextBtn(inputFields[nowInputF].text,playstop);
        };
        audioPSBtn.GetComponent<Button_Sprite>().ClickFunc = () => {         //TF變換
            playstop=!playstop;
            if(playstop==true)
            {
                PSdisplayText.SetText("P");
                PSdisplayText.ForceMeshUpdate();
            }
            else
            {
                PSdisplayText.SetText("S");
                PSdisplayText.ForceMeshUpdate();
            }
        };
        #endregion



        InvokeRepeating("DisplayCode",1.0f,1.5f);
    }

    private void Update() {

        if(Input.GetKeyDown(KeyCode.CapsLock))
        {
            changeLetter=!changeLetter;
        }

        if(Input.GetKeyDown(KeyCode.Backspace))
        {
            string tmpString=TFinputstrings[nowInputF];
            TFinputstrings[nowInputF]="";
            for(int i=0;i<tmpString.Length-1;i++)
            {
                TFinputstrings[nowInputF]+=tmpString[i];
            }
            ForceUpdateTMPInputField(inputFields[nowInputF],TFinputstrings[nowInputF]);
        }

        for(int i=0;i<5;i++)
        {
            if(activeInputs[i]==true && changeLetter==false)
            {
                GetInputNum(i);
            }
            else if(activeInputs[i]==true && changeLetter==true)
            {
                GetInputNumBig(i);
            }
        }


    }

    void ControlAll(List<GameObject> tmpList,bool truefalse)
    {
        foreach (var tmpGameObject in tmpList)
        {
            tmpGameObject.SetActive(truefalse);
        }
    }
    void CloseActiveInputs()    //一開始START初始關閉   以及輸出鈕全部關閉
    {
        for(int i=0;i<5;i++)
        {
            activeInputs[i]=false;
        }
    }

    
    void GetInput()
    {
        
        if(Input.GetKeyDown(KeyCode.A))
        {
            m_TFinputstrs += 'a';
            ForceUpdateInputField();

        }
        if(Input.GetKeyDown(KeyCode.B))
        {
            m_TFinputstrs += 'b';
            ForceUpdateInputField();
        }
        if(Input.GetKeyDown(KeyCode.N))
        {
            m_TFinputstrs += 'n';
            ForceUpdateInputField();
        }

    }


    void GetInputNum(int index)
    {
        
        if(Input.GetKeyDown(KeyCode.A))
        {
            TFinputstrings[index] += 'a';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);

        }
        if(Input.GetKeyDown(KeyCode.B))
        {
            TFinputstrings[index] += 'b';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.C))
        {
            TFinputstrings[index] += 'c';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.D))
        {
            TFinputstrings[index] += 'd';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.E))
        {
            TFinputstrings[index] += 'e';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.F))
        {
            TFinputstrings[index] += 'f';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.G))
        {
            TFinputstrings[index] += 'g';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.H))
        {
            TFinputstrings[index] += 'h';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.I))
        {
            TFinputstrings[index] += 'i';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.J))
        {
            TFinputstrings[index] += 'j';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.K))
        {
            TFinputstrings[index] += 'k';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.L))
        {
            TFinputstrings[index] += 'l';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.M))
        {
            TFinputstrings[index] += 'm';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.N))
        {
            TFinputstrings[index] += 'n';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.O))
        {
            TFinputstrings[index] += 'o';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.P))
        {
            TFinputstrings[index] += 'p';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.Q))
        {
            TFinputstrings[index] += 'q';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            TFinputstrings[index] += 'r';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.S))
        {
            TFinputstrings[index] += 's';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.T))
        {
            TFinputstrings[index] += 't';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.U))
        {
            TFinputstrings[index] += 'u';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.V))
        {
            TFinputstrings[index] += 'v';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.W))
        {
            TFinputstrings[index] += 'w';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.X))
        {
            TFinputstrings[index] += 'x';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.Y))
        {
            TFinputstrings[index] += 'y';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.Z))
        {
            TFinputstrings[index] += 'z';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }

    }

    private void GetInputNumBig(int index)
    {
        
        if(Input.GetKeyDown(KeyCode.A))
        {
            TFinputstrings[index] += 'A';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);

        }
        if(Input.GetKeyDown(KeyCode.B))
        {
            TFinputstrings[index] += 'B';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.C))
        {
            TFinputstrings[index] += 'C';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.D))
        {
            TFinputstrings[index] += 'D';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.E))
        {
            TFinputstrings[index] += 'E';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.F))
        {
            TFinputstrings[index] += 'F';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.G))
        {
            TFinputstrings[index] += 'G';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.H))
        {
            TFinputstrings[index] += 'H';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.I))
        {
            TFinputstrings[index] += 'I';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.J))
        {
            TFinputstrings[index] += 'J';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.K))
        {
            TFinputstrings[index] += 'K';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.L))
        {
            TFinputstrings[index] += 'L';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.M))
        {
            TFinputstrings[index] += 'M';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.N))
        {
            TFinputstrings[index] += 'N';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.O))
        {
            TFinputstrings[index] += 'O';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.P))
        {
            TFinputstrings[index] += 'P';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.Q))
        {
            TFinputstrings[index] += 'Q';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            TFinputstrings[index] += 'R';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.S))
        {
            TFinputstrings[index] += 'S';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.T))
        {
            TFinputstrings[index] += 'T';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.U))
        {
            TFinputstrings[index] += 'U';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.V))
        {
            TFinputstrings[index] += 'V';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.W))
        {
            TFinputstrings[index] += 'W';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.X))
        {
            TFinputstrings[index] += 'X';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.Y))
        {
            TFinputstrings[index] += 'Y';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }
        if(Input.GetKeyDown(KeyCode.Z))
        {
            TFinputstrings[index] += 'Z';
            ForceUpdateTMPInputField(inputFields[index],TFinputstrings[index]);
        }

    }
    void ForceUpdateInputField()
    {
        TFinputFieldText.SetText(m_TFinputstrs);
        TFinputFieldText.ForceMeshUpdate();
    }

    void ForceUpdateTMPInputField(TextMeshPro TFinputFieldText,string m_TFinputstrs)
    {
        TFinputFieldText.SetText(m_TFinputstrs);
        TFinputFieldText.ForceMeshUpdate();
    }

    void TextBtn(string a,bool truefalse)
    {
        // if(truefalse==true)
        // {
        //     File.AppendAllText("C:\\Users\\user\\test.txt", a+".SetActive(true);"+"\n",Encoding.Default);
        // }
        // else
        // {
        //     File.AppendAllText("C:\\Users\\user\\test.txt", a+".SetActive(false);"+"\n",Encoding.Default);
        // }


        if(truefalse==true)
        {
            File.AppendAllText("C:\\Users\\user\\test.txt", a+".SetActive(true);"+"\n",Encoding.Default);
        }
        else
        {
            File.AppendAllText("C:\\Users\\user\\test.txt", a+".SetActive(false);"+"\n",Encoding.Default);
        }
    }

    private string GetJoke() {
        string[] messageArray = new string[] {
            "I was wondering why the ball was getting bigger, then it hit me",
            "Did you hear about the guy whose whole left side was cut off? He’s all right now",
            "I'm reading a book about anti-gravity. It's impossible to put down!",
            "Don't trust atoms. They make up everything!",
            "What did the pirate say on his 80th birthday? AYE MATEY",
            "What’s Forrest Gump’s password? 1forrest1",
            "Two guys walk into a bar, the third one ducks.",
            "How many tickles does it take to make an octopus laugh? Ten-tickles",
            "Our wedding was so beautiful, even the cake was in tiers.",
            "What do you call a dinosaur with a extensive vocabulary? A thesaurus."
        };

        return messageArray[Random.Range(0, messageArray.Length)];
    }

    //讀取檔案
    void ReadFile(string FileName) {
        strs = File.ReadAllLines(FileName);//讀取檔案的所有行，並將資料讀取到定義好的字元陣列strs中，一行存一個單元
        for (int i = 0; i < strs.Length; i++)
        {
            m_Str += strs[i];//讀取每一行，並連起來
            m_Str += "\n";//每一行末尾換行
        }
    }


    void DisplayCode()
    {
        

        ReadFile("C:\\Users\\user\\test.txt");
        textMeshPro.SetText(m_Str);
        textMeshPro.ForceMeshUpdate();

        m_Str="";
    }

}
