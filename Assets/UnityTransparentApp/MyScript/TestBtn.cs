﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using System.IO;
using System.Text;

using TMPro;

public class TestBtn : MonoBehaviour
{
    public Button testBtn;

    string m_Str;
    string m_FileName;                             //法二要用
    string[] strs;                                        //法二要用

    public InputField inputField; 

    public TextMeshPro textMeshPro;


    public Button TMPButton;
    public TextMeshProUGUI TMPInputField;
    public Toggle TMPtoggle;
    
    // Start is called before the first frame update
    void Start()
    {
        // testBtn.onClick.AddListener(()=>{
        //     File.AppendAllText("C:\\Users\\user\\test.txt", "\n我被寫進來了\n",Encoding.Default);
        //     //Debug.Log("testBtn");
        // });

        testBtn.onClick.AddListener(delegate{ TextBtn("apple");});
        TMPButton.onClick.AddListener(delegate{ TMPBtn("apple",TMPtoggle.isOn);});

        // m_FileName = "Z800虛擬頭盔說明書連結UTF-8.txt";                       //法二要用，要讀取的檔名，這個是相對路徑

        


        // StreamWriter sw = File.AppendText(@"C:\Users\user\test.txt");
        // sw.WriteLine("This");
        // sw.WriteLine("is Extra");
        // sw.WriteLine("Text");
    
    
        InvokeRepeating("DisplayCode",1.0f,1.5f);
    
    }

    void TextBtn(string a)
    {
        File.AppendAllText("C:\\Users\\user\\test.txt", "\n"+"GameObject"+" "+"tmpParameter"+"="+"GameObject.Find("+a+")"+"\n",Encoding.Default);
    }

    void TMPBtn(string a,bool truefalse)
    {
        if(truefalse==true)
        {
            File.AppendAllText("C:\\Users\\user\\test.txt", "\n"+a+".SetActive(true);"+"\n",Encoding.Default);
        }
        else
        {
            File.AppendAllText("C:\\Users\\user\\test.txt", "\n"+a+".SetActive(false);"+"\n",Encoding.Default);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    // void ReadFile(string FileName) {
    //     strs = File.ReadAllLines(FileName);//讀取檔案的所有行，並將資料讀取到定義好的字元陣列strs中，一行存一個單元
    //     for (int i = 0; i < strs.Length; i++)
    //     {
    //         m_Str += strs[i];//讀取每一行，並連起來
    //         m_Str += "\n";//每一行末尾換行
    //     }
    // }



    void ReadFile(string FileName) {
        strs = File.ReadAllLines(FileName);//讀取檔案的所有行，並將資料讀取到定義好的字元陣列strs中，一行存一個單元
        for (int i = 0; i < strs.Length; i++)
        {
            m_Str += strs[i];//讀取每一行，並連起來
            m_Str += "\n";//每一行末尾換行
        }
    }

    void DisplayCode()
    {
        

        ReadFile("C:\\Users\\user\\test.txt");
        textMeshPro.SetText(m_Str);
        textMeshPro.ForceMeshUpdate();

        m_Str="";
    }
}
