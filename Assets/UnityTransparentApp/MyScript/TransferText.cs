﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TransferText : MonoBehaviour
{
    public TextMeshProUGUI placeholderText;
    public TextMeshProUGUI textText;

    string holdText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        holdText=textText.text;
        placeholderText.SetText(holdText);
        placeholderText.ForceMeshUpdate();

    }
}
